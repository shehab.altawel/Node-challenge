const fs = require('fs');
const path = require('path');

const file_path = path.join(__dirname,"dir.json");

if ( fs.existsSync(file_path) ) {
    let dictionary  = JSON.parse(fs.readFileSync(file_path));
    const operate = (method, key, value) => {
        method === 'add'  ? (dictionary[key] = value || '', console.log(`${key} = ${value} are added`)):
        method === 'list' ? console.log(dictionary) :
        method === 'get'  ? console.log(dictionary[key]) :
        method === 'remove' ? (delete dictionary[key], console.log(`${key} is deleted`)):
        method === 'clear' ? (dictionary = {} , console.log('File is cleared.')) :
        console.log(`Command ${method} is invalid command`);    
  }
  
    operate(process.argv[2], process.argv[3], process.argv[4]);
    fs.writeFileSync(file_path,JSON.stringify(dictionary));
  }